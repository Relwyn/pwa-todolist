# Todo

## TD

Réaliser une PWA Todo list avec les fonctionnalité suivantes:
* Ajouter une tache avec un titre et une description et un état réalisé
* Sauvegarder les todos en DB
* Créer/Lire les todos offline
* Synchroniser les todos créés/modifsié hors ligne à la recupération de la connexion
* Bonus : Avoir une page de détail de todo

## Libs
* JSON-server (https://github.com/typicode/json-server)
* IndexedDB (https://github.com/jakearchibald/idb)
* Tailwind CSS (https://tailwindcss.com/)
* Page.js (https://visionmedia.github.io/page.js/)

https://prod.liveshare.vsengsaas.visualstudio.com/join?8569B4A37CB45EE9D59F51C2A8725BFE86C6